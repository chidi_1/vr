// прелоадер
function preloader_animation() {
    $('.preloader-square-top').addClass('open');
    $('.preloader-square-bottom').addClass('open');
    $.cookie('visited', 'true', {expires: 10});

    setTimeout(function () {
        $('.preloader-letter-top').addClass('open visible');
        $('.preloader-letter-bottom').addClass('open visible');
    }, 2000);

    setTimeout(function () {
        $('.preloader-square-top').addClass('red');
        $('.preloader-square-bottom').addClass('red');
        $('.preloader-letter-top').addClass('red');
        $('.preloader-letter-bottom').addClass('red');
    }, 4000);

    setTimeout(function () {
        $('.preloader').addClass('hidden-block');
        $('.page-wrap').removeClass('hidden');
    }, 6000);
}

// постраничный слайдер
function init_fullscreen_slider() {
    var slider = $('.fullpage');
    var anchors_content = [];

    slider.find('.section').each(function () {
        anchors_content.push(String($(this).index()));
    });

    slider.fullpage({
        css3: true,
        scrollingSpeed: 500,
        lockAnchors: true,
        anchors: anchors_content,
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: anchors_content,
        showActiveTooltip: true,
        afterRender: function () {
            if ($('.space-slider').length) {
                $('.space-slider').owlCarousel({
                    margin: 0,
                    loop: true,
                    nav: true,
                    dots: false,
                    autoplay: true,
                    navText: [,],
                    autoplayHoverPause: false,
                    autoplayTimeout: 3000,
                    items: 1
                });

            }
        }
    });
}

var month_names_declension = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

// инициализация выбора даты
function init_calendar() {
    var cur_date = $(new Date())[0];
    var input = $('.js--inp-date');
    var date_min;
    var date_max;

    !input.data('min') ? date_min = null : date_min = $(new Date())[0];
    !input.data('min') ? date_max = null : date_max = (date_min.getDate() + Number(input.data('max')));


    set_calendar_data(cur_date);

    if ($('.js--inp-date').length) {
        $('.js--inp-date').datepicker({
            minDate: date_min,
            maxDate: date_max,
            dateFormat: 'dd.mm.yy',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь','Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            onSelect: function () {
                var start = $('.js--inp-date').val(), end = $('.js--inp-date').val();
                var stArr = start.split('.'), endArr = end.split('.');
                var new_date = new Date(stArr[2], parseInt(stArr[1]) - 1, parseInt(stArr[0]));
                var text_date = new_date.getDate() + ' ' + month_names_declension[new_date.getMonth()] + ' ' + new_date.getFullYear();
                $('.js--inp-date').val(text_date)
                set_calendar_data(new_date);
                get_date_data(input);
            }
        });
    }
}

// установка даты
function set_calendar_data(date) {
    var prev_date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1);
    var next_date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);
    var prev_month = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());
    var next_month = new Date(date.getFullYear(), date.getMonth() + 1, date.getDate());
    var prev_year = new Date(date.getFullYear() - 1, date.getMonth(), date.getDate());
    var next_year = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate());

    $('.calendar-date .prev').text(prev_date.getDate());
    $('.calendar-date .current').text(date.getDate());
    $('.calendar-date .next').text(next_date.getDate());

    $('.calendar-month .prev').text(month_names[prev_month.getMonth()]);
    $('.calendar-month .current').text(month_names[date.getMonth()]);
    $('.calendar-month .next').text(month_names[next_month.getMonth()]);

    $('.calendar-year .prev').text(prev_year.getFullYear());
    $('.calendar-year .current').text(date.getFullYear());
    $('.calendar-year .next').text(next_year.getFullYear());
}

var month_names = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"];

// табы
//$(document).on('click', 'ul.tabs--caption > li:not(.active)', function (i) {
//    var link = $(this);
//    console.log(link.index());
//    $(this).click(function(){
//        $(this).addClass('active').siblings().removeClass('active')
//          .closest('div.tabs').find('.tabs--content-wrap div.tabs__content').removeClass('active').eq(i).addClass('active');
//      });
//
//    if (link.closest('form').hasClass('form-booking')) {
//        switch_booking_step();
//    }
//});
//
//$(document).on('click', 'ul.tabs-inner--caption li', function () {
//    var link = $(this);
//    console.log(link)
//    //$('ul.tabs-inner--caption li').removeClass('active')
//    //link.addClass('active').closest('.tabs-inner').find('.tabs-inner--content').removeClass('active').eq(link.index()).addClass('active');
//
//    if (link.closest('form').hasClass('form-booking')) {
//        switch_booking_step();
//    }
//});


// слайдер внутренняя страница первый таб
function init_inner_slider() {
    var owl = $('.inner-slider');
    var owl_item;

    var is_tablet = device.tablet();
    var is_mobile = device.mobile();

    var disable_mousewheel = false;

    owl.owlCarousel({
        margin: 0,
        loop: false,
        nav: true,
        dots: true,
        autoplay: false,
        navText: [,],
        items: 1,
        onInitialized: function () {
            $('.inner-slider .owl-dot').each(function () {
                var index = Number($(this).index()) + 1;
                $(this).html('<span>' + index + '</span>');
            });
            owl_item = $('.inner-slider .owl-item .inner-content');
        },
        onTranslate: function () {
            activate_control(1000);
        },
        onTranslated: function () {
            activate_control(1000);
        }
    });

    owl.on('mousewheel', function (e) {

        var active_el_index = $('.inner-slider .owl-dots .owl-dot.active').index();

        if (disable_control == false) {
            disable_control = true;

            if (e.deltaY > 0) {
                owl.trigger('prev.owl');
            } else {
                owl.trigger('next.owl');
            }

            activate_control(1000);
        }
        //activate_control(1000);
        e.preventDefault();
    });


    owl_item.swipe({
        swipe: function (e, direction, distance, duration, fingerCount, fingerData) {

            if (direction == 'up') {
                setTimeout(function () {
                    owl.trigger('next.owl');
                }, 100);
            }
            if (direction == 'down') {
                setTimeout(function () {
                    owl.trigger('prev.owl');
                }, 100);
            }
            if (direction == null) {
                if (is_tablet || is_mobile) {
                    event.target.click();
                }
            }
        },
        threshold: 30
    });
}

// скрыть/показать декоративную страницу
function show_overlay() {
    $('.fullpage').removeClass('hidden');
}

function hide_overlay() {
    $('.fullpage').addClass('hidden');
}

function activate_control(delay) {
    setTimeout(function () {
        disable_control = false;
    }, delay)
}

// скроллбар
function init_scrollbar() {
    $.os = {name: (/(win|mac|linux|sunos|solaris|iphone)/.exec(navigator.platform.toLowerCase()) || [u])[0].replace('sunos', 'solaris')};
    if ($.os.name == 'iphone' && $('window').width() < 360) {
        $('.scrollbar').mCustomScrollbar();
    }
}

// слайдер фоновых изображений
function init_life_slider() {
    $('.life-slider').owlCarousel({
        margin: 0,
        loop: true,
        nav: true,
        dots: false,
        autoplay: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: [,],
        mouseDrag: false,
        touchDrag: false,
        items: 1
    });
}


var styles = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#212121"
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#212121"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#bdbdbd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#181818"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#1b1b1b"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#2c2c2c"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#8a8a8a"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#373737"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#3c3c3c"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#4e4e4e"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#3d3d3d"
            }
        ]
    }
];

function init_map() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.890, lng: 151.274},
        zoom: 12,
        styles: styles
    });

    var beachMarker = new google.maps.Marker({
        position: {lat: -33.890, lng: 151.274},
        map: map
    });
}
// Параллакс
window.onload = function () {
    if (document.getElementById('box') != null) {
        var parallax_box = document.getElementById('box');

        var c1left = document.getElementById('l1').offsetLeft,
            c1top = document.getElementById('l1').offsetTop;


        parallax_box.onmousemove = function (event) {
            event = event || window.event;
            var x = event.clientX - parallax_box.offsetLeft,
                y = event.clientY - parallax_box.offsetTop;

            mouse_parallax('l1', c1left, c1top, x, y, 35);
        }
    }

    if (document.getElementById('boxspace') != null) {
        var parallax_box = document.getElementById('boxspace');

        var s1left = document.getElementById('s1').offsetLeft,
            s1top = document.getElementById('s1').offsetTop,
            s2left = document.getElementById('s2').offsetLeft,
            s2top = document.getElementById('s2').offsetTop,
            s3left = document.getElementById('s3').offsetLeft,
            s3top = document.getElementById('s3').offsetTop,
            s4left = document.getElementById('s4').offsetLeft,
            s4top = document.getElementById('s4').offsetTop,
            s5left = document.getElementById('s5').offsetLeft,
            s5top = document.getElementById('s5').offsetTop,
            s6left = document.getElementById('s6').offsetLeft,
            s6top = document.getElementById('s6').offsetTop,
            s7left = document.getElementById('s7').offsetLeft,
            s7top = document.getElementById('s7').offsetTop,
            s8left = document.getElementById('s8').offsetLeft,
            s8top = document.getElementById('s8').offsetTop,
            s9left = document.getElementById('s9').offsetLeft,
            s9top = document.getElementById('s9').offsetTop;


        parallax_box.onmousemove = function (event) {
            event = event || window.event;
            var x = event.clientX - parallax_box.offsetLeft,
                y = event.clientY - parallax_box.offsetTop;

            mouse_parallax('s1', s1left, s1top, x, y, 35);
            mouse_parallax('s2', s2left, s2top, x, y, 15);
            mouse_parallax('s3', s3left, s3top, x, y, 100);
            mouse_parallax('s4', s4left, s4top, x, y, 45);
            mouse_parallax('s5', s5left, s5top, x, y, 55);
            mouse_parallax('s6', s6left, s6top, x, y, 25);
            mouse_parallax('s7', s7left, s7top, x, y, 75);
            mouse_parallax('s8', s8left, s8top, x, y, 15);
            mouse_parallax('s9', s9left, s9top, x, y, 85);
        }
    }
};

function mouse_parallax(id, left, top, mouseX, mouseY, speed) {
    var obj = document.getElementById(id);
    var parentObj = obj.parentNode,
        containerWidth = parseInt(parentObj.offsetWidth),
        containerHeight = parseInt(parentObj.offsetHeight);
    obj.style.left = left - ( ( ( mouseX - ( parseInt(obj.offsetWidth) / 2 + left ) ) / containerWidth ) * speed ) + 'px';
    obj.style.top = top - ( ( ( mouseY - ( parseInt(obj.offsetHeight) / 2 + top ) ) / containerHeight ) * speed ) + 'px';
}

// фото и видео
function init_space_gallery() {
    var grid = $('.space-content-grid');

    grid.imagesLoaded().progress(function () {
        grid.isotope();
    });

    $('.space-filters li').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        var filterValue = $(this).data('filter');
        grid.isotope({
            filter: filterValue
        });
    });
}
var games_slider;
var games_html;

function init_games_slider() {
    $('.games').addClass('desktop');
    var game_length = $('.games-slider--el').length;
    $('.position-slider').empty();
    var step;

    if (game_length < 10) {
        step = 3
    }
    else {
        if (game_length < 40) {
            step = 5
        }
        else {
            if (game_length < 100) {
                step = 10
            }
            else {
                step = 20
            }
        }
    }

    var position_length = Math.ceil(game_length / step);
    var html = '';

    if (position_length > 3) {
        html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + position_length + '">' + (step * (position_length) - (step - 1)) + '-' + game_length + '</a>';
        html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + 1 + '">' + 1 + '-' + step + '</a>';
        html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + 2 + '">' + (step + 1) + '-' + (step * 2) + '</a>';

        $('.position-slider-container').removeClass('hidden-block');
        $('.position-slider').html(html);
    }
    else {
        $('.position-slider-container').addClass('hidden-block')
    }

    $('.games-slider--el').each(function () {
        var game = $(this);
        game.css({'background-image': game.data('bgdesktop')});
        var html = '<ul class="game-content--tags">' + game.find('.game-content--tags').html() + '</ul>';
        game.find('.game-content--tags').remove();
        game.find('.game-content--top').append(html);
    });

    if ($('.games-slider--el').length > 3) {

        games_slider = $(".games-slider");
        $('.games-arrows').removeClass('hidden-block');

        games_slider.owlCarousel({
            margin: 0,
            stagePadding: 0,
            loop: true,
            nav: true,
            navText: [,],
            dots: true,
            autoplay: false,
            items: 1,
            onTranslated: function () {
                if (position_length > 3) {
                    set_position();
                }

                setTimeout(function () {
                    disable_control = false;
                }, 500);
            }
        });
    }

    var disable_control = false;

    $(document).on('click', '.js--game-next', function(){
        var active_el_index = $('.games-slider .owl-dots .owl-dot.active').index();
        var step_inner = 3;
        games_slider.trigger('to.owl.carousel', active_el_index + step_inner);

        return false;
    });
    $(document).on('click', '.js--game-prev', function(){
        var active_el_index = $('.games-slider .owl-dots .owl-dot.active').index();
        var step_inner = 3;
        games_slider.trigger('to.owl.carousel', active_el_index - step_inner);
        return false;
    });

    games_slider.on('mousewheel', function (e) {
        var active_el_index = $('.games-slider .owl-dots .owl-dot.active').index();

        if (disable_control == false) {
            disable_control = true;
            need_slide = false;

            if ($(window).width() > 1440) {
                var step_inner = 3
            }
            else {
                if ($(window).width() > 1024) {
                    step_inner = 2
                }
                else {
                    step_inner = 1;
                }
            }

            if (e.deltaY > 0) {
                games_slider.trigger('to.owl.carousel', active_el_index - step_inner);
            } else {
                games_slider.trigger('to.owl.carousel', active_el_index + step_inner);
            }
        }
        e.preventDefault();
    });

    $(document).on('click', '.js--slide-games', function () {
        var index_clicked = Number($(this).data('step'));
        games_slider.trigger('to.owl.carousel', (((index_clicked - 1) * step)));

        return false;
    });

    function set_position() {
        var index = Number(games_slider.find('.owl-item.active').find('.game-number').text());
        var current_position = Math.ceil(index / step);
        var html = '';

        if (index <= step) {
            html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + position_length + '">' + (step * (position_length) - (step - 1)) + '-' + game_length + '</a>';
            html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + current_position + '">' + 1 + '-' + step + '</a>';
            html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + (current_position + 1) + '">' + (step + 1) + '-' + (step * 2) + '</a>';
        }
        else {
            if (index > game_length - step) {
                html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + (current_position - 1) + '">' + (step * (position_length - 1) - (step - 1)) + '-' + (step * (position_length) - step) + '</a>';
                html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + current_position + '">' + (step * (position_length) - (step - 1)) + '-' + game_length + '</a>';
                html = html + '<a href="" class="position-slider--el  js--slide-games"  data-step="' + 1 + '">' + 1 + '-' + step + '</a>';
            }
            else {
                html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + (current_position - 1) + '">' + (((current_position - 2) * step) + 1) + '-' + ((current_position - 1) * step) + '</a>';
                html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + current_position + '">' + (((current_position - 1) * step) + 1) + '-' + (step * current_position) + '</a>';
                html = html + '<a href="" class="position-slider--el  js--slide-games"  data-step="' + (current_position + 1) + '">' + (((current_position) * step) + 1) + '-' + ((current_position + 1) * step) + '</a>';
            }
        }

        $('.position-slider').html(html);
    }
}

function init_games_list() {
    $('.games').removeClass('desktop');
    $('.games-slider-container').html(games_html);
    $('.games-arrows').addClass('hidden-block');

    $('.games-slider--el').each(function () {
        var game = $(this);
        game.css({'background-image': game.data('bgmobile')});
        var html = '<ul class="game-content--tags">' + game.find('.game-content--tags').html() + '</ul>';
        game.find('.game-content--tags').remove();
        game.find('.game-content-wrap').append(html);
    })
}

function show_slider_info() {
    $('.games-slider--el').each(function () {
        $(this).find('.slide-wrap').slideDown(0);
        $(this).find('.game-content--btns').fadeIn(0);
    });
}

function hide_slider_info() {
    $('.games-slider--el').each(function () {
        $(this).find('.slide-wrap').slideUp(0);
        $(this).find('.game-content--btns').fadeOut(0);
    });
}

function init_filter() {
    // �������������� �������

    var min = $('.slider-horisontal').data('min');
    var max = $('.slider-horisontal').data('max');

    var slider = document.getElementById('slider-players');

    noUiSlider.create(slider, {
        connect: true,
        step: 1,
        behaviour: 'tap-drag',
        start: [1],
        range: {
            'min': min,
            'max': max
        }
    });

    slider.noUiSlider.on('update.one', function (values) {
        var val = Number(values[0]);
        val = val.toFixed(0);
        $('.inp-players').val(val);
        $('.members-list li').removeClass('active').eq(val - 1).addClass('active');
    });

    $(document).on('click', '.js--change-members', function () {
        var link = $(this);
        link.addClass('active').siblings().removeClass('active');
        slider.noUiSlider.set([link.text()]);
        $('.inp-players').val(link.text());
    });

    // �������� ������
    $(document).on('click', '.js--clear-filter', function () {
        $('.label-checkbox input').removeAttr('checked');
        $('.label-radio input').removeAttr('checked');
        slider.noUiSlider.set(1);
        $('.filter-players .members-list li').removeClass('active');
        $('.filter-players .members-list li:first-child').addClass('active');

        $('.select-styled option').removeAttr('selected');
        $('.select-styled option:first-child').attr('selected', 'selected');

        $('.jq-selectbox__dropdown ul li').removeClass('selected sel');
        $('.jq-selectbox__dropdown ul li:first-child').addClass('selected sel');
        $('.jq-selectbox__select-text').text($('.filter-section .jq-selectbox__dropdown ul li:first-child').text());
        //$('.select-styled .jq-selectbox__dropdown ul li:first-child').trigger('click');
        set_filter();

        return false;
    });
}


function check_games_view() {
    if (is_tablet == false && is_mobile == false) {
        if ($(window).width() < 800) {
            init_games_list();
        }
        else {
            if ($(window).width() <= 1024) {
                if (!$('.games-slider.owl-carousel').length) {
                    init_games_slider(function () {
                        show_slider_info();
                    });
                }
                else {
                    show_slider_info();
                }
            }
            else {
                if (!$('.games-slider.owl-carousel').length) {
                    init_games_slider(function () {
                        hide_slider_info();
                    });
                }
                else {
                    hide_slider_info();
                }
            }
        }
    }
    else {
        if (is_tablet) {
            if (window.orientation == 90) {
                init_games_slider();
            }
            if (window.orientation == 0) {
                init_games_list();
            }
        }
        else {
            init_games_list();
        }
    }
}

function init_vertical_slider() {
    //var current = $('.inp-members');
    var min = $('.slider-vertical').data('min');
    var max = $('.slider-vertical').data('max');

    var slider = document.getElementById('slider');

    noUiSlider.create(slider, {
        connect: true,
        orientation: 'vertical',
        step: 1,
        direction: 'rtl',
        behaviour: 'tap-drag',
        start: [1],
        range: {
            'min': min,
            'max': max
        }
    });

    slider.noUiSlider.on('update.one', function(values){
        var val = Number(values[0]);
        val = val.toFixed(0);
        $('.inp-members').val(val);
        $('.members-list li').removeClass('active').eq(val - 1).addClass('active');
        count_time_price();
        refresh_booking_data();
    });

    $(document).on('click', '.js--change-members', function () {
        var link = $(this);
        link.addClass('active').siblings().removeClass('active');
        slider.noUiSlider.set([link.text()]);
        $('.inp-members').val(link.text());

        count_time_price();
    });
}

function init_style_select() {
    $('.js--select-styled').styler({
        onSelectClosed: function () {
            if ($(this).closest('form').hasClass('form-booking')) {
                var option = $(this).find('.jq-selectbox__dropdown ul li.selected');
            }
        }
    });
}

function set_mask() {
    $('.inp-phone').mask("+7 (999) 999 99 99", {
        completed: function () {
            check_input_data();
        }
    });
}

// переключение табов в форме бронирования
function switch_booking_step(direction) {
    var index = $('.form-booking .tabs--caption li.active').index();
    var index_text = index + 1;
    direction == 'next' ? index++ : index--;
    direction == 'next' ? index_text++ : index_text--;

    $('.step').html(index_text + " <i>—</i> 5");
    $('.form-booking .tabs--caption li.active').removeClass('active');
    $('.form-booking .tabs--caption li').eq(index).removeClass('disabled').addClass('active');
    $('.form-booking .tabs--content').removeClass('active').eq(index).addClass('active');
    check_booking_btns();
}

// проверить состояние кнопок
function check_booking_btns() {
    var index = $('.form-booking .tabs--caption li.active').index();

    if (index == 0) {
        $('.js--prev').addClass('hidden-block');
        $('.index-booking-btns').addClass('one-btn');
    }
    else {
        if (index == 4) {
            $('.js--next').addClass('hidden-block');
            $('.js--form-submit').removeClass('hidden-block');
        }
        else {
            $('.index-booking-btns').removeClass('one-btn');
            $('.js--prev').removeClass('hidden-block');
            $('.js--next').removeClass('hidden-block disabled');
            $('.js--form-submit').addClass('hidden-block');

            if (index == 2) {
                if (help_video_is_showed == false) {
                    help_video_is_showed = true;
                    $('.params-popup').addClass('open');
                }

                if ($('.js--change-time:checked').length != 0) {
                    $('.js--next').removeClass('disabled');
                }
                else {
                    $('.js--next').addClass('disabled');
                }

                check_chioced_time();
            }

            if (index == 3) {
                check_input_data();
            }
        }
    }
}

// проверка выбранного времени
function check_chioced_time() {
    if ($('.js--change-time:checked').length != 0) {
        $('.js--next').removeClass('disabled');
    }
    else {
        $('.js--next').addClass('disabled');
    }
}

// проверка введенных данных
function check_input_data() {
    $('.index-booking-info .inp.required').each(function () {
        var error = false;
        if ($(this).val() == '' || $(this).val() == 'undefined') {
            error = 'true';
        }

        if (error == 'true') {
            $('.js--next').addClass('disabled');
        }
        else {
            $('.js--next').removeClass('disabled');
        }
    })
}

// стоимость посещения при выборе времени
function count_time_price() {
    var members = Number($('.inp-members').val());
    var price = 0;
    $('.time-list input:checked').each(function () {
        price = price + Number($(this).data('price')) * members;
    });
    var time = Number($('.time-list input:checked').length);
    $('.time-total-min').text(time * 30 + ' мин');
    $('.time-total-val').text(price + ' руб');

    refresh_booking_data()
}

// проверка скидки
function check_discount(input) {
    var action = input.data('action');
    var method = input.data('method');
    var data = input.val();

    $.ajax({
        url: action,
        method: method,
        data: {data: data},
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);
            if (parse_data.error == false) {
                input.addClass('error')
            }
            else {
                $('.val-discount strong').text(parse_data.value);
                refresh_booking_data();
            }
        }
    })
}


// финальные данные
function refresh_booking_data() {
    var form = $('.form-booking');
    var time = Number($('.time-list input:checked').length);
    var price = time * 500 * Number($('.params-members input').val());
    var members = form.find('.params-members input').val();

    if (members == 2 || members == 3 || members == 4) {
        var members_text = ' человека';
    }
    else {
        var members_text = ' человек';
    }

    // имя
    if (form.find('.inp-name').val() != '' || form.find('.inp-name').val() != 'undefined') {
        $('.index-booking-finish h2 .name').text(form.find('.inp-name').val() + ',');
    }
    // телефон
    $('.index-booking-finish .val-phone strong').text(form.find('.inp-phone').val());
    // дата
    $('.index-booking-finish .val-date strong').text(form.find('.inp-date').val());
    // время
    $('.index-booking-finish .val-time strong').text($('.js--change-time:checked:first-child').val());
    // Оборудование
    $('.index-booking-finish .val-eq strong').text(form.find('.js--change-eq:checked').val());
    // игроки
    $('.index-booking-finish .val-members strong').text(members + members_text);
    // длительность
    $('.index-booking-finish .val-durability strong').text(time * 30 + ' мин');
    // цена
    $('.index-booking-finish .val-price strong').text(price + ' руб');
    // итоговая цена
    if (price != 0) {
        $('.index-booking-finish .val-total strong').text(price - Number($('.inp-discount').data('discount')) + ' руб');
    }
    else {
        $('.index-booking-finish .val-total strong').text('0 руб');
    }
}

// отправка даты с календаря
function get_date_data(input) {
    var action = input.data('action');
    var method = input.data('method');
    var data = input.val();

    $.ajax({
        url: action,
        method: method,
        data: {data: data},
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);

            if (input.hasClass('js--load-life')) {
                $('.team-list ul.life-table').empty().html(parse_data.html);
                init_scrollbar();
            }

            if (input.hasClass('js--load-time')) {
                $('.time-list').empty().html(parse_data.html);
                $('.time-total-min').text('0 мин');
                $('.time-total-val').text('0 руб');
            }
        }
    })
}

// показать инфрмацию об игре
function show_game_info(el) {
    var link = el;
    var action = link.data('action');
    var method = link.data('method');
    var data = link.data('id');

    $.ajax({
        url: action,
        method: method,
        data: {'data': data},
        success: function (data) {
            $('.game-info-container').html(data);
            $('.game-info-container').addClass('open');
            $('body').addClass('fixed');
            init_scrollbar();
        }
    });
}

// фильтр

function set_filter(close) {
    var form = $('.filter-form');
    var action = form.attr('action');
    var method = form.attr('method');
    var data = form.serialize();

    $.ajax({
        url: action,
        method: method,
        data: data,
        success: function (data) {
            games_html = data;
            $('.games-arrows').addClass('hidden-block');
            $('.games-slider-container').html(games_html);
            check_games_view();
            if ($('.games-slider').height() < $('.game-filter').height()) {
                $('.games-slider-container').addClass('show-center')
            }
            else {
                $('.games-slider-container').removeClass('show-center')
            }

            if (close == 'true') {
                $('.close-filter.js--close-block').trigger('click');
            }
        }
    })
}

// ищем инфрмацию об игре
function search_games() {
    var form = $('.search-form');
    var action = form.prop('action');
    var method = form.prop('method');
    var data = form.serialize();

    $.ajax({
        url: action,
        method: method,
        data: data,
        success: function (data) {
            games_html = data;
            $('.games-arrows').addClass('hidden-block');
            $('.games-slider-container').empty().html(games_html);
            check_games_view();
            if ($('.games-slider').height() < $('.game-filter').height()) {
                $('.games-slider-container').addClass('show-center')
            }
            else {
                $('.games-slider-container').removeClass('show-center')
            }
        }
    });
}
var disable_control = false;
var help_video_is_showed = false;
var is_tablet;
var is_mobile;

$(document).ready(function () {
    is_tablet = device.tablet();
    is_mobile = device.mobile();

    // прелоадер
    //if ($.cookie('visited') == undefined) {
    //    preloader_animation();
    //}
    //else {
    $('.preloader').addClass('hidden-block');
    $('.page-wrap').removeClass('hidden');
    //}

    // СЛАЙДЕРЫ
    // основной полноэкранный слайдер
    if ($('.fullpage').length) {
        init_fullscreen_slider();
    }
    // внутренний слайдер в табах
    if ($('.inner-slider').length) {
        init_inner_slider();
    }

    if ($('.scrollbar').length) {
        init_scrollbar();
    }

    // Скрыть декоративный слайдер
    var decor_block = $('.js--hide-overlay');

    decor_block.on('mousewheel', function (e) {
        if (e.deltaY < 0) {
            hide_overlay();
        }
        e.preventDefault();
    });

    if (is_tablet || is_mobile) {
        decor_block.swipe({
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                if (direction == 'up' && !disable_control) {
                    disable_control = true;
                    hide_overlay();
                    activate_control(1000);
                }
                if (direction == null) {
                    event.target.click();
                }
            },
            threshold: 10
        });
    }

    // показать декоративный слайдер
    var open_block = $('.js--show-overlay');

    open_block.on('mousewheel', function (e) {
        if (e.deltaY > 0) {
            show_overlay();
        }
        e.preventDefault();
    });

    if (is_tablet || is_mobile) {
        open_block.swipe({
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                if (direction == 'down') {
                    show_overlay();
                }
                if (direction == null) {
                    event.target.click();
                }
            },
            threshold: 10
        });
    }

    // попапы
    $(document).on('click', '.js--open-block', function () {
        $($(this).attr('href')).addClass('open');
        $('body').addClass('fixed');
        return false;
    });

    $(document).on('click', '.js--close-block', function () {
        $($(this).attr('href')).removeClass('open');
        $('body').removeClass('fixed');
        return false;
    });

    // PRICE
    // смена оборудования в форме бронирования
    $(document).on('click', '.js--set-eq', function () {
        $('.inp-eq').val($(this).data('eq'));
    });

    // LIFE
    if ($('.calendar-wrap').length) {
        init_calendar();
    }

    // слайдер фоновых изображений в Life
    if ($('.life-slider').length) {
        init_life_slider();
    }

    // КОННТАКТЫ
    if ($('#map').length) {
        init_map();
    }

    $(document).on('click', '.js--toggle-panel', function () {
        $('.contacts-panel').toggleClass('open');
        return false;
    });

    // ГЛАВНАЯ
    // форма бронирования
    if ($('.inp-phone').length) {
        set_mask();
    }
    $('.index-booking-info .required').on('blur', function () {
        check_input_data();
        refresh_booking_data();
    });

    // выпадающий список
    if ($('.js--select-styled').length) {
        init_style_select();
    }

    // перекрытие инпута с датй на мобильном
    $('.inp-date-mobile-overlay').on('click', function () {
        $('.inp-date').trigger('focus');
    });

    // слайдер с участниками
    if ($('.slider-vertical').length) {
        init_vertical_slider();
    }

    // выбор времени посещения
    $(document).on('click', '.js--change-time', function () {
        check_chioced_time();
        count_time_price();
    });

    // выбор оборудования
    $(document).on('change', '.form-booking .js--change-eq', function () {
        refresh_booking_data();
    });

    // применить скидку
    $('.inp-discount').blur(function () {
        check_discount($(this));
    });

    // переключене табов в форме бронирования
    $(document).on('click', '.js--next', function () {
        switch_booking_step('next');
        return false;
    });

    $(document).on('click', '.js--prev', function () {
        switch_booking_step('prev');
        return false;
    });

    // ЦЕНА
    $(document).on('click', '.js--change-eq', function () {
        $('.inp-eq').val($(this).data('eq'));
    });

    // SPACE
    if ($('.space-content-grid').length) {
        init_space_gallery();
    }

    // GAMES
    // слайдер с играми
    if ($('.games-slider').length) {
        games_html = $('.games-slider-container').html();
        check_games_view();
        init_filter();

        $(window).resize(function () {
            check_games_view();
        });

        window.addEventListener("orientationchange", function () {
            check_games_view();
        }, false);
    }

    // наведение на игру
    $(document).on('mouseenter', '.games-slider--el', function () {
        if ($(window).width() > 1024) {
            var container = $(this).closest('.games-slider--el');
            container.find('.slide-wrap').stop().slideDown(200);
            container.find('.game-content--btns').stop().fadeIn(200);
        }

    });
    $(document).on('mouseleave', '.games-slider--el', function () {
        if (!(device.mobile() == false && device.tablet() == false && $(window).width() > 1024)) {
            return false;
        }
        if ($(window).width() > 1024) {
            var container = $(this).closest('.games-slider--el');
            container.find('.slide-wrap').stop().slideUp(200);
            container.find('.game-content--btns').stop().fadeOut(200);
        }
    });

    // форма поиска
    $('.js--open-search').on('click', function () {
        $('.search-form').toggleClass('open');
        return false;
    });

    $(document).on('click', '.js--search-submit', function () {
        search_games();
        return false;
    });

    $(document).on('submit', '.search-form', function () {
        $('.inp-search').blur();
        search_games();

        return false;
    });

    // применить фильтр
    $(document).on('click', '.js--set-filter', function () {
        set_filter('true');
        return false;
    });

    // показать информацию об игре
    $(document).on('click', '.js--game-info', function () {
        show_game_info($(this));
        return false;
    });

    // планшеты/мбильные показать текст полностью
    $(document).on('click', '.js--toggle-game-about', function () {
        var btn = $(this);
        $('.mobile-text').fadeToggle(300);
        btn.toggleClass('hide');
        if (btn.hasClass('hide')) {
            btn.text(btn.data('close'))
        } else {
            btn.text(btn.data('open'))
        }
        return false;
    });

    $('ul.tabs--caption').on('click', 'li:not(.active)', function (i) {
        $(this).addClass('active').siblings().removeClass('active');
        $(this).closest('.tabs').find('div.tabs--content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('ul.tabs-inner--caption').on('click', 'li:not(.active)', function () {
        $(this).addClass('active').siblings().removeClass('active').closest('.tabs-inner').find('.time-list .tabs-inner--content').removeClass('active').eq($(this).index()).addClass('active');
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('.inp').on('focus', function(){
        $(this).removeClass('error')
    });

    $('.js--form-submit').on('click', function () {
        var btn = $(this);
        var form = btn.closest('.main-form');
        var errors = false;

        $(form).find('.inp.required').each(function () {
            var inp = $(this);
            var val = inp.prop('value');
            if (val == '' || val == undefined) {
                inp.addClass('error');
                errors = true;
            }
            else {
                if (inp.hasClass('inp-mail')) {

                    if (validateEmail(val) == false) {
                        inp.addClass('error');
                        errors = true;
                    }
                }
            }
        });

        if (errors == false) {
            var button_value = btn.text();
            btn.text('Подождите...');

            var method = form.attr('method');
            var action = form.attr('action');
            var data = form.serialize();

            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function (data) {
                    form.find('.inp').each(function () {
                        $(this).prop('value', '')
                    });
                    if(form.hasClass('form-booking')){
                        var parse_data = jQuery.parseJSON(data);
                        window.location = parse_data.url;
                    }
                    else{
                        form.closest('section').removeClass('open');
                        $('.section-sended').addClass('open');
                    }
                },
                error: function (data) {
                    btn.text('Ошибка');
                    setTimeout(function () {
                        $(form).find('.js-form-submit').text(button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });
});